// alert('Hello Mediasoft')

var canvas = document.getElementById("canvas");
 var score_holder = document.getElementById("score_holder")
 var ctx = canvas.getContext("2d")
 var width = canvas.width;
 var height = canvas.height;
 var blockSize = 10;
 var countBlocksInWidth = width / blockSize;
 var countBlocksInHeight = height / blockSize;
 var score = 0;
 var defaultImage = document.getElementById('source');
 var customImage = document.getElementById('source1');
 // Переменная определяет, наступил ли конец игры
 var gameIsOver = false;

 var drawBorder = function() {
   ctx.fillStyle = "Gray";
   ctx.fillRect(0, 0, width, blockSize);
   ctx.fillRect(0, height-blockSize, width, blockSize);
   ctx.fillRect(0, 0, blockSize, height);
   ctx.fillRect(width-blockSize, 0, blockSize, height);
 }
  var drawScore = function (){
    score_holder.innerHTML = "<b>Счет:</b>" + score;
  }
  var gameOver = function (){

    clearInterval(intervalId);
    ctx.font = "60px Courier";
    ctx.fillStyle = "Black";
    ctx.textAlign = "center";
    ctx.textBaseLine = "middle";
    ctx.fillText("Конец игры", width/2, height/4);
    ctx.fillText("Хочешь еще?", width/2, height/3);
    ctx.fillText("Нажми: R", width/2, height/2);
    // Задаем конец игры
    gameIsOver = true
  }
  var circle = function (x, y, radius, fillCircle){
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2, false);
    if (fillCircle){
      ctx.fill();
    } else {
      ctx.stroke();
    }
  }

    var Block = function (col,row,id) {
      this.col=col;
      this.row=row;
      this.id=id;
    }

    Block.prototype.drawDefaultImage = function () {
      var x = this.col * blockSize;
      var y = this.row * blockSize;
      var id = this.id;
      ctx.drawImage(defaultImage, x, y, blockSize, blockSize);
    }


    Block.prototype.drawCustomImage = function (angle) {
        var x = this.col * blockSize;
        var y = this.row * blockSize;
        var id = this.id;
        ctx.drawImage(customImage, x, y, blockSize, blockSize);
      }

    Block.prototype.drawCircle = function (color) {
      var centerX = this.col * blockSize + blockSize/2;
      var centerY = this.row * blockSize + blockSize/2;
      ctx.fillStyle = color;
      circle(centerX,centerY,blockSize/2,true);
    }
    Block.prototype.equal = function (otherBlock) {
      return this.col === otherBlock.col && this.row === otherBlock.row;
    }
    // Выносим дефолтные значения в отдельную переменную
    var defaultSnakeSegments = [
      new Block(7,5, 0),
      new Block(6,5, 1),
      new Block(5,5, 1)
    ];
    var Snake = function () {
      this.segments = defaultSnakeSegments;
      this.direction = "right";
      this.nextDirection = "right";
    }

    Snake.prototype.draw = function (angle) {
        this.segments[0].drawCustomImage(angle);

      for (var i=1; i< this.segments.length; i++){
        this.segments[i].drawDefaultImage();
      }
    }

    Snake.prototype.move = function () {
      var head = this.segments[0];
      var newHead;
      this.direction=this.nextDirection;

      if (this.direction==="right") {
        newHead = new Block(head.col+1, head.row, 1);
      } else if (this.direction==="down") {
        newHead = new Block(head.col, head.row+1, 1);
      } else if (this.direction==="left") {
        newHead = new Block(head.col-1, head.row, 1);
      } else if (this.direction==="up") {
        newHead = new Block(head.col, head.row-1, 1);
      }
      if (this.checkCollision(newHead)) {
        gameOver();
        return;
      }
      this.segments.unshift(newHead);
      if (newHead.equal(apple.position)) {
        score++;
        apple.move();
        return
      } else {
        this.segments.pop();
      }
    }

    Snake.prototype.checkCollision = function (head) {
      var leftCollision = (head.col === 0);
      var topCollision = (head.row === 0);
      var rightCollision = (head.col === countBlocksInWidth-1);
      var bottomCollision = (head.row === countBlocksInHeight-1);
      var wallCollision = leftCollision || topCollision || rightCollision||
      bottomCollision;
      var selfCollision = false;
      for (var i = 0; i < this.segments.length; i++) {
        if (head.equal(this.segments[i])) {
          selfCollision = true;
        }
      }
      return wallCollision || selfCollision;
    }
    Snake.prototype.setDirection = function (newDirection) {
      if (this.direction === "up" && newDirection === "down") {
        return;
      } else if (this.direction === "right" && newDirection === "left") {
        return;
      } else if (this.direction === "down" && newDirection === "up") {
        return;
      }else if (this.direction === "left" && newDirection === "right") {
        return;
      }
      this.nextDirection = newDirection;
    }
    var Apple = function (){
      this.position = new Block (10,10,0);
    }
    Apple.prototype.draw = function(){
      this.position.drawCircle("LimeGreen");
    }
    Apple.prototype.move = function () {
      var randomCol = Math.floor(Math.random()*(countBlocksInWidth-2))+1;
      var randomRow = Math.floor(Math.random()*(countBlocksInHeight-2))+1;
      this.position = new Block(randomCol,randomRow);
    }
    var snake = new Snake();
    var apple = new Apple();
    // выносим логику игры в отдельную функцию
    var snakeGame = function(angle) {
      ctx.clearRect(0, 0, width, height);
      drawScore();
      snake.move();
      snake.draw(snake.nextDirection);
      apple.draw();
      drawBorder();
    }
    var intervalId = setInterval(snakeGame, 100);

    var directions = {
      37:"left",
      38:"up",
      39:"right",
      40:"down"
    }

    // кнопка R
    var resetbutton = 82;

    $("body").keydown(function(event) {
      var newDirection = directions[event.keyCode];
      if(newDirection !== undefined) {
        snake.setDirection(newDirection);
      };
      // Перезапускаем игру по нажатию кнопки R, если наступил геймовер
      if (gameIsOver && (event.keyCode == resetbutton)) {
        document.location.reload();
      }
    });
